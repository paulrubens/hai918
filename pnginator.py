from PIL import Image
import os
import sys

def convert_to_png(folder_path):
    for filename in os.listdir(folder_path):
        if filename.endswith('.pgm') or filename.endswith('.ppm'):
            try:
                img_path = os.path.join(folder_path, filename)
                img = Image.open(img_path)

                new_filename = os.path.splitext(filename)[0] + '.png'
                new_img_path = os.path.join(folder_path, new_filename)

                if not os.path.exists(new_img_path):
                    img.save(new_img_path)
                    print(f"Image {filename} converted to {new_filename}")
                else:
                    print(f"File {new_filename} already exists. Skipped.")

            except Exception as e:
                print(f"An error occurred while converting {filename}: {e}")

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python3 pnginator.py folder_path")
        sys.exit(1)

    folder_path = sys.argv[1]
    if not os.path.exists(folder_path):
        print(f"The folder {folder_path} does not exist.")
        sys.exit(1)

    convert_to_png(folder_path)
