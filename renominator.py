import os
import sys

def rename_files_in_directory(directory_path):
    # Liste tous les fichiers dans le répertoire
    all_files = os.listdir(directory_path)
    
    # Trie les fichiers et filtre uniquement les fichiers .pgm
    pgm_files = sorted([f for f in all_files if f.endswith('.pgm')])

    # Vérifie s'il y a des fichiers .pgm dans le dossier
    if not pgm_files:
        print("Aucun fichier .pgm trouvé dans le répertoire.")
        return

    # Renomme les fichiers
    for idx, filename in enumerate(pgm_files, 1):
        old_file_path = os.path.join(directory_path, filename)
        new_file_path = os.path.join(directory_path, f"{idx}.pgm")
        
        os.rename(old_file_path, new_file_path)
        print(f"Renommé {old_file_path} en {new_file_path}")

if __name__ == "__main__":
    # Vérifie si un argument a été fourni
    if len(sys.argv) < 2:
        print("Veuillez fournir le chemin vers le répertoire contenant les images .pgm.")
        sys.exit(1)

    directory_path = sys.argv[1]
    
    # Vérifie si le chemin du répertoire est valide
    if not os.path.isdir(directory_path):
        print("Le chemin fourni n'est pas un répertoire valide.")
        sys.exit(1)
        
    rename_files_in_directory(directory_path)
