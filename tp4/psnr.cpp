#include <cstdio>
#include "image_ppm.h"
#include <random>
#include <cstdlib>
#include <iostream>

int main(int argc, char *argv[]) {

    struct couleur {
        unsigned long rouge;
        unsigned long vert;
        unsigned long bleu;
    };

    char cNomImgLue[250], cNomImgLue2[250];
    int nH, nW, nTaille;

    if (argc != 3) {
        printf("Usage: ImageIn.pgm ImgOut.pgm\n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgLue2);

    OCTET *ImgIn, *ImgIn2;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    allocation_tableau(ImgIn2, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    lire_image_pgm(cNomImgLue2, ImgIn2, nH * nW);


    /*
     * PSNR: différence entre ImgIn et ImgOut
     */

    couleur EQM = {0, 0, 0};
    double EQMt;
    std::cout << "calcul de l'EQM" << std::endl;
    for (int i = 0; i < nTaille; i++) {
        double nR = static_cast<double>(ImgIn[i]);
        double nR2 = static_cast<double>(ImgIn2[i]);
        EQM.rouge += ((nR - nR2) * (nR - nR2));
    }
    EQMt = EQM.rouge;
    EQMt /= nTaille;

    std::cout << "calcul PSNR" << std::endl;

    if (EQMt == 0) {
        std::cout << "Les images sont identiques. PSNR = inf dB" << std::endl;
        return 1;
    }

    double PSNR = 10 * std::log10(std::pow(255, 2) / EQMt);
    std::cout << "PSNR = " << PSNR << " dB" << std::endl;

    return 1;
}
