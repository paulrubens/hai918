#include <stdio.h>
#include "image_ppm.h"

int main(int argc, char* argv[])
{
  char cNomImgLue[250], cNomImgEcrite[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.pgm \n"); 
       exit (1) ;
     }
 
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomImgEcrite);

   OCTET *ImgIn, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   allocation_tableau(ImgOut, OCTET, nTaille);
	
   //   for (int i=0; i < nTaille; i++)
   // {
   //  if ( ImgIn[i] < S) ImgOut[i]=0; else ImgOut[i]=255;
   //  }


 /*for (int i=0; i < nH; i++)
   for (int j=0; j < nW; j++)
     {
       if ( ImgIn[i*nW+j] < S) ImgOut[i*nW+j]=255; else ImgOut[i*nW+j]=0;
     }*/


      for(int i = 0; i < nH; i++){
        for(int j = 0; j < nW; j++){
            //un max par couleur
            int cpt = 0;
            int gauss;
            int div = 0;
            for(int col = -1; col<2; col++){
                for(int lig = -1; lig <2; lig++){
                  if(i+col >=0 && i+col < nH && j+lig >=0 && j+lig < nW){
                    if(col == lig && col == 0){
                      gauss = 4;
                    }
                    else if(col == lig || col == -lig){
                      gauss = 1;
                    }
                    else{
                      gauss = 2;
                    }
                    cpt += gauss*ImgIn[(i+col)*nW+(j+lig)];
                    div += gauss;
                  }

                }
            }
            ImgOut[i*(nW)+j] = cpt/div;
        }
    }


   ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); free(ImgOut);

   return 1;
}
