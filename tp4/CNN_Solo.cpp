//
// Created by florian.ravet-lecourt@etu.umontpellier.fr on 02/10/23.
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
#include <string>
#include <vector>
#include <chrono>
#include <thread>

//passe-haut
int filtre1 [9] = {1,1,1,
                   0,0,0,
                   -1,-1,-1};

//passe-haut
int filtre2 [9] = {0,1,1,
                   -1,0,1,
                   -1,-1,0};

//passe-haut
int filtre3 [9] = {-1,0,1,
                   -1,0,1,
                   -1,0,1};

//laplacien
int filtre4 [9] = {0,1,0,
                   -1,4,-1,
                   0,1,0};

//sobel
int filtre5 [9] = {-1,-1,-1,
                   -1,8,-1,
                   -1,-1,-1};

//gauss
int filtre6 [9] = {1,-2,1,
                   -2,4,-2,
                   1,-2,1};

//passe-bas
int filtre7 [9] = {0,1,0,
                   -1,5,-1,
                   0,1,0};

//passe-haut
int filtre8 [9] = {-1,-1,0,
                   -1,0,1,
                   0,1,1};

//passe-haut
int filtre9 [9] = {-1,-1,-1,
                   0,0,0,
                   1,1,1};

//passe-haut
int filtre10 [9] = {0,-1,-1,
                   1,0,-1,
                   1,1,0};

//passe-haut
int filtre11 [9] = {1,0,-1,
                   1,0,-1,
                   1,0,-1};

//passe-haut
int filtre12 [9] = {1,1,0,
                   1,0,-1,
                   0,-1,-1};

// passe-bas, filtre moyenneur
int filtre13[9] = {1, 1, 1,
                   1, 1, 1,
                   1, 1, 1};

// passe-bas, filtre gaussien
int filtre14[9] = {1, 2, 1,
                   2, 4, 2,
                   1, 2, 1};

// passe-bas
int filtre15[9] = {0, 1, 0,
                   1, 2, 1,
                   0, 1, 0};

int *filtres[15] = {filtre1, filtre10, filtre11, filtre12, filtre13, filtre14,
                   filtre15, filtre2, filtre3, filtre4, filtre5, filtre6,
                   filtre7, filtre8, filtre9};

int num_filtre = 0;

using namespace std;

void convolution(OCTET *ImgIn, std::vector<std::vector<OCTET>> &resultat, int nH, int nW, int début_index) {
    for (int f = 0; f < 3; f++) {
        int *filtre = filtres[(début_index + f) % 15];
        std::vector<OCTET> layer(nH * nW, 0);
        for (int i = 1; i < nH - 1; i++) {
            for (int j = 1; j < nW - 1; j++) {
                int valeur = 0;
                int index = 0;
                for (int x = -1; x <= 1; x++) {
                    for (int y = -1; y <= 1; y++) {
                        valeur += ImgIn[(i+x) * nW + (j+y)] * filtre[index++];
                    }
                }
                // ReLU
                layer[i * nW + j] = max(0, valeur);
            }
        }
        resultat.push_back(layer);
    }
}

void pooling(std::vector<OCTET> &ImgIn, std::vector<OCTET> &resultat, int nH, int nW) {
    for (int i = 0; i < nH; i+=2) {
        for (int j = 0; j < nW; j+=2) {
            int somme = 0;
            for (int x = 0; x <= 1; x++) {
                for (int y = 0; y <= 1; y++) {
                    somme += ImgIn[(i+x) * nW + (j+y)];
                }
            }
            resultat[(i/2) * (nW/2) + (j/2)] = max(0, somme / 4);
        }
    }
}

std::vector<long double> CNN(std::string path) {
    // Reading the image
    int nH, nW, nTaille;
    lire_nb_lignes_colonnes_image_pgm((char *)path.c_str(), &nH, &nW);
    nTaille = nH * nW;
    OCTET *ImgIn, *ImgOut;
    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm((char *)path.c_str(), ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    std::vector<std::vector<OCTET>> resultat;

    // First Convolution and Pooling
    convolution(ImgIn, resultat, nH, nW, num_filtre);
    //std::cout << "First Convolution Done." << std::endl;
    nH -= 2; // Update dimensions
    nW -= 2;
    std::vector<std::vector<OCTET>> resultatPooled1;
    for (auto &layer : resultat) {
        std::vector<OCTET> pooled_layer((nH / 2) * (nW / 2));
        pooling(layer, pooled_layer, nH, nW);
        resultatPooled1.push_back(pooled_layer);
    }
    //std::cout << "First Pooling Done." << std::endl;
    nH /= 2; // Update dimensions
    nW /= 2;

    if(nH % 2 != 0 || nW % 2 != 0){
        nH -= 1;
        nW -= 1;
    }

    // Second Convolution and Pooling
    std::vector<std::vector<OCTET>> resultat2;
    for(auto &layer : resultatPooled1) {
        OCTET* newImgIn = &layer[0];
        convolution(newImgIn, resultat2, nH, nW, num_filtre);
    }
    //std::cout << "Second Convolution Done." << std::endl;
    nH -= 2;
    nW -= 2;
    std::vector<std::vector<OCTET>> resultatPooled2;
    for (auto &layer : resultat2) {
        std::vector<OCTET> pooled_layer((nH / 2) * (nW / 2));
        pooling(layer, pooled_layer, nH, nW);
        resultatPooled2.push_back(pooled_layer);
    }
    //std::cout << "Second Pooling Done." << std::endl;
    nH /= 2;
    nW /= 2;
    if(nH % 2 != 0 || nW % 2 != 0){
        nH -= 1;
        nW -= 1;
    }

    // Third Convolution and Pooling
    std::vector<std::vector<OCTET>> resultat3;
    for(auto &layer : resultatPooled1) {
        OCTET* newImgIn = &layer[0];
        convolution(newImgIn, resultat3, nH, nW, num_filtre);
    }
    //std::cout << "Third Convolution Done." << std::endl;
    nH -= 2;
    nW -= 2;
    std::vector<std::vector<OCTET>> resultatPooled3;
    for (auto &layer : resultat3) {
        std::vector<OCTET> pooled_layer((nH / 2) * (nW / 2));
        pooling(layer, pooled_layer, nH, nW);
        resultatPooled3.push_back(pooled_layer);
    }
    //std::cout << "Third Pooling Done." << std::endl;
    nH /= 2;
    nW /= 2;
    if(nH % 2 != 0 || nW % 2 != 0){
        nH -= 1;
        nW -= 1;
    }

    // Fourth Convolution and Pooling
    std::vector<std::vector<OCTET>> resultat4;
    for(auto &layer : resultatPooled1) {
        OCTET* newImgIn = &layer[0];
        convolution(newImgIn, resultat4, nH, nW, num_filtre);
    }
    //std::cout << "Fourth Convolution Done." << std::endl;
    nH -= 2;
    nW -= 2;
    std::vector<std::vector<OCTET>> resultatPooled4;
    for (auto &layer : resultat4) {
        std::vector<OCTET> pooled_layer((nH / 2) * (nW / 2));
        pooling(layer, pooled_layer, nH, nW);
        resultatPooled4.push_back(pooled_layer);
    }
    //std::cout << "Fourth Pooling Done." << std::endl;
    nH /= 2;
    nW /= 2;
    if(nH % 2 != 0 || nW % 2 != 0){
        nH -= 1;
        nW -= 1;
    }

    // Fifth Convolution and Pooling
    std::vector<std::vector<OCTET>> resultat5;
    for(auto &layer : resultatPooled1) {
        OCTET* newImgIn = &layer[0];
        convolution(newImgIn, resultat5, nH, nW, num_filtre);
    }
    //std::cout << "Fifth Convolution Done." << std::endl;
    nH -= 2;
    nW -= 2;
    std::vector<std::vector<OCTET>> resultatPooled5;
    for (auto &layer : resultat5) {
        std::vector<OCTET> pooled_layer((nH / 2) * (nW / 2));
        pooling(layer, pooled_layer, nH, nW);
        resultatPooled5.push_back(pooled_layer);
    }
    //std::cout << "Fifth Pooling Done." << std::endl;
    nH /= 2;
    nW /= 2;
    if(nH % 2 != 0 || nW % 2 != 0){
        nH -= 1;
        nW -= 1;
    }

    std::vector<long double> vecteur_final;
    for(int i = 0; i < resultatPooled5.size(); i++){
        for(int j = 0; j < nH*nW; j++){
            vecteur_final.push_back((long double) resultat5[i][j]/255.0);
        }
    }

    return vecteur_final;
}

long double fully_connected(std::vector<long double> &input, std::vector<long double> &weights, long double length) {

    long double sum = 0;

    for(int i = 0; i < input.size(); i++) {
        sum += input[i] * weights[i];
    }

    return sum;
}

int main(int argc, char* argv[]){

    char cNomImgLue[250];//, cNomImgEcrite[250];
    //int nH, nW, nTaille;

    if (argc != 2)
    {
        printf("Usage: ImageIn.pgm ImageOut.txt \n"); //modifier pour prendre toutes les images?
        exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLue) ;
    /*sscanf (argv[2],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);*/

    std::string all_birds = "./Birds/";
    std::string all_koalas = "./Koalas/";
    std::vector<std::vector<long double>> photo_bird;
    std::vector<std::vector<long double>> photo_koala;
    int nbPhotos = 300; //grand nombre d'échantillons

    std::cout << "Analyse des images d'oiseaux" << std::endl;
    for (int i = 0; i < nbPhotos; i++) {
        std::vector<long double> data = CNN(all_birds + std::to_string(i) + ".pgm");
        photo_bird.push_back(data);

        std::cout << "\rProgression : " << i+1 << " / " << nbPhotos << std::flush;
    }
    std::cout << std::endl;

    std::cout << "Analyse des images de koalas" << std::endl;
    for (int i = 0; i < nbPhotos; i++) {
        std::vector<long double> data = CNN(all_koalas + std::to_string(i) + ".pgm");
        photo_koala.push_back(data);

        std::cout << "\rProgression : " << i+1 << " / " << nbPhotos << std::flush;
    }
    std::cout << std::endl;

    std::vector<long double> poids_bird, poids_koala;
    int tailleTotale = photo_bird[0].size();
    std::vector<long double> vecteur_char(2); //n = 2 car nous avons deux classes: bird et koala
    std::vector<long double> vecteur_char_err(2);

    for(int i = 0; i < tailleTotale; i++){ //le début se fait avec des poids aléatoire, ces poids seront adaptés lors de l'itération
        poids_bird.push_back((long double) std::rand() / RAND_MAX);
        poids_koala.push_back((long double) std::rand() / RAND_MAX);
    }

    std::cout << "Création du vecteur fully_connected" << std::endl;

    float lambda = 0.00001; //ajout d'un lambda pour nos calculs
    int nbIter = 50;

    for(int iter = 0; iter < nbIter; iter++){
        for(int i = 0; i < nbPhotos; i++){
            int j = i; //pour calcul sur deuxième classe

            //d'abord les oiseaux
            vecteur_char[0] = fully_connected(photo_bird[i], poids_bird, tailleTotale) + 1;
            vecteur_char[1] = fully_connected(photo_bird[i], poids_koala, tailleTotale) + 1;

            long double sommeTotale = vecteur_char[0] + vecteur_char[1];
            vecteur_char[0] /= sommeTotale; vecteur_char[1] /= sommeTotale;

            vecteur_char_err[0] = 1 - vecteur_char[0];
            vecteur_char_err[1] = 0 - vecteur_char[1];

            //adaptation des poids
            for (int p = 0; p < tailleTotale; p++){
                poids_bird[p] = poids_bird[p] + lambda * photo_bird[i][p] * vecteur_char_err[0];
                poids_koala[p] = poids_koala[p] + lambda * photo_bird[i][p] * vecteur_char_err[1];
            }

            //ensuite, les koalas
            vecteur_char[0] = fully_connected(photo_koala[j], poids_bird, tailleTotale) + 1;
            vecteur_char[1] = fully_connected(photo_koala[j], poids_koala, tailleTotale) + 1;

            sommeTotale = vecteur_char[0] + vecteur_char[1];
            vecteur_char[0] /= sommeTotale; vecteur_char[1] /= sommeTotale;

            vecteur_char_err[0] = 0 - vecteur_char[0];
            vecteur_char_err[1] = 1 - vecteur_char[1];

            //adaptation des poids
            for (int p = 0; p < tailleTotale; p++){
                poids_bird[p] = poids_bird[p] + lambda * photo_koala[j][p] * vecteur_char_err[0];
                poids_koala[p] = poids_koala[p] + lambda * photo_koala[j][p] * vecteur_char_err[1];
            }

        }
        std::cout << "\rJ'en suis à l'itération n°" << iter+1 << ", il reste " << nbIter - iter - 1 << " iterations." << std::flush;
    }
    std::cout << std::endl;

    //faire un test sur les autres images pour tester réseau de neuronnes
    std::cout << "Test de l'image d'entrée" << std::endl;

    std::vector<long double> data = CNN(cNomImgLue);

    vecteur_char[0] = fully_connected(data, poids_bird, tailleTotale) + 1;
    vecteur_char[1] = fully_connected(data, poids_koala, tailleTotale) + 1;

    long double sommeTotale = vecteur_char[0] + vecteur_char[1];
    vecteur_char[0] /= sommeTotale; vecteur_char[1] /= sommeTotale;

    std::cout << "L'image a " << vecteur_char[0] * 100 << "% de chance d'être un oiseau" <<std::endl;
    std::cout << "L'image a " << vecteur_char[1] * 100 << "% de chance d'être un koala" <<std::endl;

    return 0;
}
