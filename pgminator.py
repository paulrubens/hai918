from PIL import Image
import os
import sys

def convert_to_pgm(folder_path):
    for filename in os.listdir(folder_path):
        if filename.endswith('.jpg') or filename.endswith('.png'):
            img_path = os.path.join(folder_path, filename)
            img = Image.open(img_path).convert('L')  # Convert to grayscale

            # Resize image
            img = img.resize((300, 300), Image.ANTIALIAS)

            # Remove the file extension and add '.pgm'
            new_filename = os.path.splitext(filename)[0] + '.pgm'
            new_img_path = os.path.join(folder_path, new_filename)

            # Save the image in PPM format (PIL does not directly support PGM)
            img.save(new_img_path, 'PPM')
            
            # Delete the original image
            os.remove(img_path)

            print(f"Image {filename} converted to {new_filename}")

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("Usage: python3 pgminator.py folder_path")
        sys.exit(1)

    folder_path = sys.argv[1]
    if not os.path.exists(folder_path):
        print(f"The folder {folder_path} does not exist.")
        sys.exit(1)

    convert_to_pgm(folder_path)
