// -------------------------------------------
// gMini : a minimal OpenGL/GLUT application
// for 3D graphics.
// Copyright (C) 2006-2008 Tamy Boubekeur
// All rights reserved.
// -------------------------------------------

// -------------------------------------------
// Disclaimer: this code is dirty in the
// meaning that there is no attention paid to
// proper class attribute access, memory
// management or optimisation of any kind. It
// is designed for quick-and-dirty testing
// purpose.
// -------------------------------------------

#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <algorithm>
#include <GL/glut.h>
#include <float.h>
#include "src/Vec3.h"
#include "src/Camera.h"
#include "platform.h"
#include <bitset>
#include "rans_byte.h"

struct Triangle {
    inline Triangle() {
        v[0] = v[1] = v[2] = 0;
    }

    inline Triangle(const Triangle &t) {
        v[0] = t.v[0];
        v[1] = t.v[1];
        v[2] = t.v[2];
    }

    inline Triangle(unsigned int v0, unsigned int v1, unsigned int v2) {
        v[0] = v0;
        v[1] = v1;
        v[2] = v2;
    }

    unsigned int &operator[](unsigned int iv) { return v[iv]; }

    unsigned int operator[](unsigned int iv) const { return v[iv]; }

    inline virtual ~Triangle() {}

    inline Triangle &operator=(const Triangle &t) {
        v[0] = t.v[0];
        v[1] = t.v[1];
        v[2] = t.v[2];
        return (*this);
    }

    // membres :
    unsigned int v[3];
};

struct Mesh {
    std::vector<Vec3> vertices;
    std::vector<Vec3> normals;
    std::vector<Triangle> triangles;
    std::vector<float> sommets;

    void buildVertexArray();

    std::vector<unsigned int> array;

    void buildTriangleArray();

    std::vector<float> Nsommets;

    void buildNormalArray();

    std::vector<float> couleurs;

    void buildColorArray();
};

Mesh mesh;
std::string message = "000111000";
bool display_normals;
bool display_loaded_mesh;

// -------------------------------------------
// OpenGL/GLUT application code.
// -------------------------------------------

static GLint window;
static unsigned int SCREENWIDTH = 1600;
static unsigned int SCREENHEIGHT = 900;
static Camera camera;
static bool mouseRotatePressed = false;
static bool mouseMovePressed = false;
static bool mouseZoomPressed = false;
static int lastX = 0, lastY = 0, lastZoom = 0;
static bool fullScreen = false;

bool saveOFF(const std::string &filename,
             std::vector<Vec3> &i_vertices,
             std::vector<Vec3> &i_normals,
             std::vector<Triangle> &i_triangles,
             bool save_normals = true) {
    std::ofstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return false;
    }

    myfile << "OFF" << std::endl;

    unsigned int n_vertices = i_vertices.size(), n_triangles = i_triangles.size();
    myfile << n_vertices << " " << n_triangles << " 0" << std::endl;

    for (unsigned int v = 0; v < n_vertices; ++v) {
        myfile << i_vertices[v][0] << " " << i_vertices[v][1] << " " << i_vertices[v][2] << " ";
        if (save_normals) myfile << i_normals[v][0] << " " << i_normals[v][1] << " " << i_normals[v][2] << std::endl;
        else myfile << std::endl;
    }
    for (unsigned int f = 0; f < n_triangles; ++f) {
        myfile << 3 << " " << i_triangles[f][0] << " " << i_triangles[f][1] << " " << i_triangles[f][2];
        myfile << std::endl;
    }
    myfile.close();
    return true;
}

void openOFF(std::string const &filename,
             std::vector<Vec3> &o_vertices,
             std::vector<Vec3> &o_normals,
             std::vector<Triangle> &o_triangles,
             bool load_normals = true) {
    std::ifstream myfile;
    myfile.open(filename.c_str());
    if (!myfile.is_open()) {
        std::cout << filename << " cannot be opened" << std::endl;
        return;
    }

    std::string magic_s;

    myfile >> magic_s;

    if (magic_s != "OFF") {
        std::cout << magic_s << " != OFF :   We handle ONLY *.off files." << std::endl;
        myfile.close();
        exit(1);
    }

    int n_vertices, n_faces, dummy_int;
    myfile >> n_vertices >> n_faces >> dummy_int;

    o_vertices.clear();
    o_normals.clear();

    for (int v = 0; v < n_vertices; ++v) {
        float x, y, z;

        myfile >> x >> y >> z;
        o_vertices.push_back(Vec3(x, y, z));

        if (load_normals) {
            myfile >> x >> y >> z;
            o_normals.push_back(Vec3(x, y, z));
        }
    }

    o_triangles.clear();
    for (int f = 0; f < n_faces; ++f) {
        int n_vertices_on_face;
        myfile >> n_vertices_on_face;

        if (n_vertices_on_face == 3) {
            unsigned int _v1, _v2, _v3;
            myfile >> _v1 >> _v2 >> _v3;

            o_triangles.push_back(Triangle(_v1, _v2, _v3));
        } else if (n_vertices_on_face == 4) {
            unsigned int _v1, _v2, _v3, _v4;
            myfile >> _v1 >> _v2 >> _v3 >> _v4;

            o_triangles.push_back(Triangle(_v1, _v2, _v3));
            o_triangles.push_back(Triangle(_v1, _v3, _v4));
        } else {
            std::cout << "We handle ONLY *.off files with 3 or 4 vertices per face" << std::endl;
            myfile.close();
            exit(1);
        }
    }

}


// ------------------------------------

void initLight() {
    GLfloat light_position1[4] = {22.0f, 16.0f, 50.0f, 0.0f};
    GLfloat direction1[3] = {-52.0f, -16.0f, -50.0f};
    GLfloat color1[4] = {1.0f, 1.0f, 1.0f, 1.0f};
    GLfloat ambient[4] = {0.3f, 0.3f, 0.3f, 0.5f};

    glLightfv(GL_LIGHT1, GL_POSITION, light_position1);
    glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, direction1);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, color1);
    glLightfv(GL_LIGHT1, GL_SPECULAR, color1);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
    glEnable(GL_LIGHT1);
    glEnable(GL_LIGHTING);
}

void init() {
    camera.resize(SCREENWIDTH, SCREENHEIGHT);
    initLight();
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);
    glDepthFunc(GL_LESS);
    glEnable(GL_DEPTH_TEST);
    glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
    glEnable(GL_COLOR_MATERIAL);
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    glEnableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_COLOR_MATERIAL);

    display_normals = false;
    display_loaded_mesh = true;
}




// ------------------------------------
// rendering.
// ------------------------------------


void drawVector(Vec3 const &i_from, Vec3 const &i_to) {

    glBegin(GL_LINES);
    glVertex3f(i_from[0], i_from[1], i_from[2]);
    glVertex3f(i_to[0], i_to[1], i_to[2]);
    glEnd();
}

void Mesh::buildVertexArray() {
    mesh.sommets.clear();
    for (unsigned int tIt = 0; tIt < mesh.vertices.size(); ++tIt) {

        mesh.sommets.push_back(mesh.vertices[tIt][0]);
        mesh.sommets.push_back(mesh.vertices[tIt][1]);
        mesh.sommets.push_back(mesh.vertices[tIt][2]);

    }
}

void Mesh::buildTriangleArray() {
    for (unsigned int tIt = 0; tIt < mesh.triangles.size(); ++tIt) {

        mesh.array.push_back(mesh.triangles[tIt][0]);
        mesh.array.push_back(mesh.triangles[tIt][1]);
        mesh.array.push_back(mesh.triangles[tIt][2]);

    }
}

void Mesh::buildNormalArray() {
    for (unsigned int tIt = 0; tIt < mesh.normals.size(); ++tIt) {

        mesh.Nsommets.push_back(mesh.normals[tIt][0]);
        mesh.Nsommets.push_back(mesh.normals[tIt][1]);
        mesh.Nsommets.push_back(mesh.normals[tIt][2]);

    }
}

void Mesh::buildColorArray() {
    for (unsigned int tIt = 0; tIt < mesh.normals.size(); ++tIt) {

        mesh.couleurs.push_back(mesh.normals[tIt][0]);
        mesh.couleurs.push_back(mesh.normals[tIt][1]);
        mesh.couleurs.push_back(mesh.normals[tIt][2]);

    }
}


//Fonction to change
void drawTriangleMesh(Mesh const &i_mesh) {


    glVertexPointer(3, GL_FLOAT, 3 * sizeof(float), (GLvoid *) (&i_mesh.sommets[0]));

    glNormalPointer(GL_FLOAT, 3 * sizeof(float), (GLvoid *) (&i_mesh.Nsommets[0]));

    glColorPointer(3, GL_FLOAT, 3 * sizeof(float), (GLvoid *) (&i_mesh.couleurs[0]));

    glDrawElements(GL_TRIANGLES, i_mesh.array.size(), GL_UNSIGNED_INT, (GLvoid *) (&i_mesh.array[0]));


}

void drawNormals(Mesh const &i_mesh) {

    glLineWidth(1.);
    for (unsigned int pIt = 0; pIt < i_mesh.normals.size(); ++pIt) {
        Vec3 to = i_mesh.vertices[pIt] + 0.02 * i_mesh.normals[pIt];
        drawVector(i_mesh.vertices[pIt], to);
    }
}

void draw() {

    if (display_loaded_mesh) {
        glColor3f(0.8, 0.8, 1);
        drawTriangleMesh(mesh);
    }

    if (display_normals) {
        glColor3f(1., 0., 0.);
        drawNormals(mesh);
    }

}

void display() {
    glLoadIdentity();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera.apply();
    draw();
    glFlush();
    glutSwapBuffers();
}

void idle() {
    glutPostRedisplay();
}

void drawQuantif(Mesh &i_mesh, int qp) {
    float maxVal[3] = {0.0f, 0.0f, 0.0f};
    for (auto &vertice: i_mesh.vertices) {
        for (int i = 0; i < 3; i++) {
            maxVal[i] = std::max(maxVal[i], std::fabs(vertice[i]));
        }
    }

    float scale[3];
    int quantizationLevels = (1 << qp) - 1;
    for (int i = 0; i < 3; i++) {
        if (maxVal[i] > 0.0f) {
            scale[i] = (float) quantizationLevels / maxVal[i];
        } else {
            scale[i] = 0.0f;
        }
    }

    for (auto &vertice: i_mesh.vertices) {
        for (int i = 0; i < 3; i++) {
            vertice[i] = std::round(vertice[i] * scale[i]) / scale[i];
        }
    }

    i_mesh.buildVertexArray();
    i_mesh.buildTriangleArray();
    i_mesh.buildNormalArray();
    i_mesh.buildColorArray();

    drawTriangleMesh(i_mesh);
    display();
}

void drawRevQuantif(Mesh &i_mesh, int qp) {

    float maxVal[3] = {0.0f, 0.0f, 0.0f};
    for (auto &vertice: i_mesh.vertices) {
        for (int i = 0; i < 3; i++) {
            maxVal[i] = std::max(maxVal[i], std::fabs(vertice[i]));
        }
    }

    float scale[3];
    int quantizationLevels = (1 << qp) - 1;
    for (int i = 0; i < 3; i++) {
        if (maxVal[i] > 0.0f) {
            scale[i] = maxVal[i] / (float) quantizationLevels;
        } else {
            scale[i] = 0.0f;
        }
    }

    for (auto &vertice: i_mesh.vertices) {
        for (int i = 0; i < 3; i++) {
            vertice[i] = std::round(vertice[i] / scale[i]) * scale[i];
        }
    }


    i_mesh.buildVertexArray();
    i_mesh.buildTriangleArray();
    i_mesh.buildNormalArray();
    i_mesh.buildColorArray();

    drawTriangleMesh(i_mesh);
    display();
}

void RMSE(Mesh &i_mesh, int qp) {

    std::vector<Vec3> original(i_mesh.vertices);
    drawQuantif(i_mesh, qp);

    double sumSquaredDistances = 0.0;
    for (size_t i = 0; i < original.size(); i++) {
        const Vec3 &originalVertex = original[i];
        const Vec3 &quantizedVertex = i_mesh.vertices[i];
        for (int j = 0; j < 3; j++) {
            double diff = originalVertex[j] - quantizedVertex[j];
            sumSquaredDistances += diff * diff;
        }
    }

    double meanSquaredDistance = sumSquaredDistances / original.size();
    std::cout << "RMSE: " << std::sqrt(meanSquaredDistance) << std::endl;
}

Vec3 calculerBarycentre(Mesh const &mesh) {
    Vec3 barycentre(0, 0, 0);
    float totalAire = 0.0f;

    for (const auto &triangle: mesh.triangles) {
        Vec3 v0 = mesh.vertices[triangle[0]];
        Vec3 v1 = mesh.vertices[triangle[1]];
        Vec3 v2 = mesh.vertices[triangle[2]];

        // Calculer l'aire du triangle
        Vec3 crossProduct = Vec3::cross(v1 - v0, v2 - v0);
        float aire = crossProduct.length() / 2.0f;

        // Calculer le centroïde du triangle
        Vec3 centroid = (v0 + v1 + v2) / 3.0f;

        // Mettre à jour le barycentre et l'aire totale
        barycentre += aire * centroid;
        totalAire += aire;
    }

    // Finaliser le calcul du barycentre
    if (totalAire != 0.0f) {
        barycentre /= totalAire;
    }

    return barycentre;
}

Vec3 cartesianToSpherical(const Vec3& cartesian) {
    float x = cartesian[0];
    float y = cartesian[1];
    float z = cartesian[2];

    float r = std::sqrt(x * x + y * y + z * z);
    float theta = std::atan2(y, x);
    float phi = std::acos(z / r);

    return Vec3(r, theta, phi);
}

Vec3 sphericalToCartesian(const Vec3& spherical) {
    float r = spherical[0];
    float theta = spherical[1];
    float phi = spherical[2];

    float x = r * std::sin(phi) * std::cos(theta);
    float y = r * std::sin(phi) * std::sin(theta);
    float z = r * std::cos(phi);

    return Vec3(x, y, z);
}

void getExtrema(Mesh const &mesh, float &minR, float &maxR) {
    minR = std::numeric_limits<float>::max();
    maxR = std::numeric_limits<float>::min();

    for (const auto &vertice : mesh.vertices) {
        Vec3 newSphere = cartesianToSpherical(vertice);
        float r = newSphere[0];
        if (r <= minR) {
            minR = r;
        }
        if (r >= maxR) {
            maxR = r;
        }
    }
}

std::vector<float> getAllMin(const std::vector<std::vector<Vec3>>& Bins) {
    std::vector<float> min(Bins.size(), std::numeric_limits<float>::max());
    for (size_t i = 0; i < Bins.size(); ++i) {
        for (const auto& vertice : Bins[i]) {
            Vec3 spherical = cartesianToSpherical(vertice);
            min[i] = std::min<float>(min[i], spherical[0]);
        }
    }
    return min;
}

std::vector<float> getAllMax(const std::vector<std::vector<Vec3>>& Bins) {
    std::vector<float> _max(Bins.size(), std::numeric_limits<float>::min());
    for (size_t i = 0; i < Bins.size(); ++i) {
        for (const auto& vertice : Bins[i]) {
            Vec3 spherical = cartesianToSpherical(vertice);
            _max[i] = std::max<float>(_max[i], spherical[0]);
        }
    }
    return _max;
}

std::vector<float> normalize(const std::vector<float>& values, float min, float max) {
    std::vector<float> normalizedValues;
    float range = max - min;

    for (const auto& value : values) {
        float normalized = (value - min) / range;
        normalizedValues.push_back(normalized);
    }

    return normalizedValues;
}

std::vector<float> denormalize(const std::vector<float>& normalizedValues, float min, float max) {
    std::vector<float> denormalizedValues;
    float range = max - min;

    for (const auto& normalized : normalizedValues) {
        float denormalized = normalized * range + min;
        denormalizedValues.push_back(denormalized);
    }

    return denormalizedValues;
}

//première version incorrecte
/*
std::map<int, std::vector<float>> creerHistogramme(Mesh const &mesh, int k) {
    // Étape 1 : Calculer le barycentre
    Vec3 barycentre = calculerBarycentre(mesh);

    // Étape 2 : Transformer les coordonnées
    std::vector<float> coordRads;
    for (const auto &vertice : mesh.vertices) {
        Vec3 relativeVertice = vertice - barycentre;
        Vec3 spherical = cartesianToSpherical(relativeVertice);
        coordRads.push_back(spherical[0]);  // Stocker la coordonnée radiale
    }

    // Étape 3 : Trouver les valeurs minimales et maximales
    float minR, maxR;
    getExtrema(mesh, minR, maxR);

    // Étape 4 : Normalisation
    std::vector<float> normaleCoordRads = normalize(coordRads, minR, maxR);

    // Étape 5 : Création des bins et insertion
    std::map<int, std::vector<float>> bins;
    for (const auto &normaleCoord : normaleCoordRads) {
        int binIndex = static_cast<int>(normaleCoord * k);
        if (binIndex >= k) {
            binIndex = k - 1;  // Pour s'assurer que l'indice est dans la plage [0, k-1]
        }
        bins[binIndex].push_back(normaleCoord);
    }

    return bins;
}

void tatouage(Mesh &mesh, const std::string &message, int numBins, float alpha, float force) {
    // Étape 1
    std::cout << "Étape 1: Calculer le barycentre" << std::endl;
    Vec3 barycentre = calculerBarycentre(mesh);

    // Étape 2
    std::cout << "Étape 2 : Transformer les coordonnées" << std::endl;
    std::map<int, std::vector<float>> bins = creerHistogramme(mesh, numBins);
    float minR, maxR;
    getExtrema(mesh, minR, maxR);

    // Étape 3
    std::cout << "Étape 3 : Insertion itérative dans chaque bin en fonction du message" << std::endl;
    int messageIndex = 0;
    for (auto &bin : bins) {
        if (messageIndex >= message.length()) {
            break;
        }

        char bit = message[messageIndex] - '0';
        float kn = (bit == 1) ? (1 - 2 * alpha) / (1 + 2 * alpha) : (1 + 2 * alpha) / (1 - 2 * alpha);

        // Étape 4 : Modifier la moyenne
        float sum = 0;
        for (const auto &normaleCoord : bin.second) {
            sum += std::pow(normaleCoord, kn);  // Utilisation de kn ici
        }
        float majMoyenne = sum / bin.second.size();

        // Étape 5 : Appliquer la force d'insertion
        for (auto &normaleCoord : bin.second) {
            normaleCoord = majMoyenne + force * (normaleCoord - majMoyenne);
        }

        // Passer au bit suivant du message
        messageIndex++;
    }

    // Étape 6
    std::cout << "Étape 6 : Dénormalisation et mise à jour du maillage" << std::endl;
    std::vector<float> majMaillage(mesh.vertices.size(), 0.0f);
    for (const auto &bin : bins) {
        for (const auto &normalizedCoord : bin.second) {
            int originalIndex = static_cast<int>(normalizedCoord * mesh.vertices.size());
            if (originalIndex >= mesh.vertices.size()) {
                originalIndex = mesh.vertices.size() - 1;
            }
            majMaillage[originalIndex] = normalizedCoord;
        }
    }

    std::vector<float> denormalizeCoordRads = denormalize(majMaillage, minR, maxR);

    for (size_t i = 0; i < mesh.vertices.size(); ++i) {
        Vec3 nouvSommet = mesh.vertices[i] - barycentre;
        Vec3 spherical = cartesianToSpherical(nouvSommet);
        spherical[0] = denormalizeCoordRads[i];
        mesh.vertices[i] = sphericalToCartesian(spherical) + barycentre;
    }
}

//fonctionnement similaire à la fonction de tatouage
std::string extraireMessage(Mesh const &mesh, int numBins, float alpha) {
    Vec3 barycentre = calculerBarycentre(mesh);

    std::map<int, std::vector<float>> bins = creerHistogramme(mesh, numBins);
    float minR, maxR;
    getExtrema(mesh, minR, maxR);

    std::string message;
    for (const auto &bin : bins) {
        float sum = 0;
        for (const auto &normalizedCoord : bin.second) {
            sum += normalizedCoord;
        }
        float moyenne = sum / bin.second.size();

        float seuil = 0.5 + alpha;
        char bit = (moyenne > seuil) ? '1' : '0';

        message.push_back(bit);
    }

    return message;
}*/

void normalizeBins(std::vector<std::vector<Vec3>>& Bins, const std::vector<float>& min, const std::vector<float>& max) {
    for(unsigned int i = 0 ; i < Bins.size() ; ++i) {
        for(unsigned int j = 0 ; j < Bins[i].size() ; ++j) {
            Bins[i][j][0] = (Bins[i][j][0] - min[i])/(std::max(max[i] - min[i], 0.001f));
        }
    }
}

void denormalizeBins(std::vector<std::vector<Vec3>>& Bins, const std::vector<float>& min, const std::vector<float>& max) {
    for(unsigned int i = 0; i < Bins.size(); ++i) {
        for(unsigned int j = 0; j < Bins[i].size(); ++j) {
            Bins[i][j][0] = Bins[i][j][0] * (std::max(max[i] - min[i], 0.001f)) + min[i];
        }
    }
}

float moy(const std::vector<Vec3>& Bin) {
    float moyenne = 0.0f;
    for(unsigned int i = 0 ; i < Bin.size() ; ++i) {
        moyenne += Bin[i][0];
    }
    moyenne /= (Bin.size() != 0 ? (float)(Bin.size()) : 1.0f);
    return moyenne;
}

std::vector<float> moyBins(const std::vector<std::vector<Vec3>>& Bins) {
    std::vector<float> moyenne(Bins.size());
    for(unsigned int i = 0 ; i < Bins.size() ; ++i) {
        moyenne[i] = moy(Bins[i]);
    }
    return moyenne;
}

void binsPow(std::vector<Vec3>& bin, float k) {
    for (unsigned int i = 0; i < bin.size(); i++) {
        bin[i][0] = std::pow(bin[i][0], k);
    }
}

void rotation(std::vector<std::vector<Vec3>>& Bins, float alpha, std::string message, float force = 1.0f, unsigned int iter = 100) {
    float k = 1.0f;
    float moyenne = 0.0f;
    unsigned int count = 0;
    for(unsigned int w = 0; w < message.size(); ++w) {
        count = 0;
        k = 1.0f;
        moyenne = moy(Bins[w]);
        bool direction = (message[w] == '1');
        if(direction) {
            while(moyenne <= (float)(1./2. + alpha) && count < iter) {
                binsPow(Bins[w], k);
                moyenne = moy(Bins[w]);
                k-=force;
                count++;
            }
        } else {
            while(moyenne > (float)(1./2. - alpha) && count < iter) {
                binsPow(Bins[w], k);
                moyenne = moy(Bins[w]);
                k+=force;
                count++;
            }
        }
    }
}

std::vector<std::vector<Vec3>> creerHistogramme(const std::vector<Vec3>& meshVertices, float min, float max, int nbins=1) {
    std::vector<std::vector<Vec3>> Bins(nbins);
    float interval = (max - min) / nbins;

    for (const auto& vertice : meshVertices) {
        for (int i = 0; i < nbins; ++i) {
            float local_min = min + interval * i;
            float local_max = min + interval * (i + 1);
            if (vertice[0] >= local_min && (vertice[0] < local_max || (i == nbins - 1 && vertice[0] <= local_max))) {
                Bins[i].push_back(vertice);
            }
        }
    }
    return Bins;
}

void tatouage(Mesh &mesh, float alpha, int nbins, float force){
    Vec3 barycentre = calculerBarycentre(mesh);
    std::vector<Vec3> spherical;

    for (const auto& vertice : mesh.vertices) {
        spherical.push_back(cartesianToSpherical(vertice - barycentre));
    }

    float minR, maxR;
    getExtrema(mesh, minR, maxR);

    std::vector<std::vector<Vec3>> Bins = creerHistogramme(spherical,minR,maxR,nbins);

    std::vector<float> minR_bin = getAllMin(Bins);
    std::vector<float> maxR_bin = getAllMax(Bins);
    normalizeBins(Bins,minR_bin,maxR_bin);

    //message est une variable globale
    rotation(Bins, alpha, message, force);

    denormalizeBins(Bins,minR_bin,maxR_bin);

    std::vector<Vec3> new_mesh;
    for (const auto& bin : Bins) {
        for (const auto& point : bin) {
            new_mesh.push_back(sphericalToCartesian(point) + barycentre);
        }
    }

    mesh.vertices = new_mesh;
}

void extraireMessage(Mesh const& mesh, float alpha, int nbins) {
    std::vector<Vec3> spherical;

    for (const auto& vertice : mesh.vertices) {
        spherical.push_back(cartesianToSpherical(vertice));
    }

    float minR, maxR;
    getExtrema(mesh, minR, maxR);

    //std::cout << "creerHistogramme extraireMessage" << std::endl;
    std::vector<std::vector<Vec3>> Bins = creerHistogramme(spherical,minR,maxR,nbins);

    // normalizeBins
    std::vector<float> minR_bin = getAllMin(Bins);
    std::vector<float> maxR_bin = getAllMax(Bins);

    //std::cout << "normlisation extraireMessage" << std::endl; //ok
    normalizeBins(Bins,minR_bin,maxR_bin);

    //std::cout << "extraction du message" << std::endl; //ok
    std::vector<float> moyenne = moyBins(Bins);
    std::string message2;
    for(unsigned int i = 0 ; i < std::min(message.size(),moyenne.size()) ; ++i){
        message2.push_back((moyenne[i] > (float)(1./2.) ? '1' : '0'));
    }

    //ecrire message dans un cout ici
    std::cout << "message extrait: " << message2 << std::endl;

    return;
}

void RMSEtrue(Mesh &i_mesh) {

    std::vector<Vec3> original(i_mesh.vertices);

    int k = message.length();
    float alpha = 0.1f;
    float force = 0.05f;
    tatouage(mesh, alpha, k, force);

    double sumSquaredDistances = 0.0;
    for (size_t i = 0; i < original.size(); i++) {
        const Vec3 &originalVertex = original[i];
        const Vec3 &quantizedVertex = i_mesh.vertices[i];
        for (int j = 0; j < 3; j++) {
            double diff = originalVertex[j] - quantizedVertex[j];
            sumSquaredDistances += diff * diff;
        }
    }

    double meanSquaredDistance = sumSquaredDistances / original.size();
    std::cout << "RMSE: " << std::sqrt(meanSquaredDistance) << std::endl;
}

std::vector<uint32_t> computeFrequencies(const std::vector<Vec3> &vertices) {
    std::vector<uint32_t> freqs(vertices.size(), 0);
    for (const auto &vertex: vertices) {
        for (int i = 0; i < 3; i++) {
            uint8_t symbol = static_cast<uint8_t>(vertex[i]);
            freqs[symbol]++;
        }
    }
    return freqs;
}

void revRans(const std::vector<uint8_t> &compressed) {

    std::cout << "début decompression" << std::endl;

    uint32_t rANSin = 0;
    for (int i = 0; i < 4; i++) {
        rANSin |= static_cast<uint32_t>(compressed[i]) << (i * 8);
    }

    std::vector<uint32_t> freqs(256, 0);
    for (const auto &comp: compressed) {
        freqs[comp]++;
    }
    freqs[0] += 1; // ajout d'un symbole supplémentaire pour éviter une division par zéro

    uint32_t cumFreqs[257] = {0};
    for (size_t i = 1; i < freqs.size(); i++) {
        cumFreqs[i] = cumFreqs[i - 1] + freqs[i - 1];
    }
    cumFreqs[256] = compressed.size() + 1;

    const uint32_t rANScarry = (1u << 31);

    uint32_t state = rANSin;

    std::vector<Vec3> vertices;
    for (size_t i = 4; i < compressed.size(); i += 3) {
        uint32_t symbol = 0;
        uint32_t freqRange = cumFreqs[256];
        while (state >= freqRange) {
            symbol++;
            freqRange = cumFreqs[symbol + 1];
        }

        const float range = cumFreqs[symbol + 1] - cumFreqs[symbol];
        const float base = state - cumFreqs[symbol];
        const float quantized = (base * range) / (cumFreqs[256] - 1);

        float x = ((quantized / (1 << 16)) - 0.5f) * 2.0f;
        float y = (((int) quantized % (1 << 16)) / (1 << 8) - 0.5f) * 2.0f;
        float z = (((int) quantized % (1 << 8)) - 0.5f) * 2.0f;

        vertices.emplace_back(x, y, z);

        state = state * freqs[symbol] + symbol - cumFreqs[symbol];
        while (state < rANScarry) {
            state = (state << 8) | compressed[i++];
        }
    }

    std::cout << "fin decompression" << std::endl;

}

void rans(Mesh &i_mesh, int qp) {

    std::cout << "deb rANS" << std::endl;

    drawQuantif(i_mesh, qp);

    /**
     * old
     */
    /*std::vector<uint32_t> freqs = computeFrequencies(i_mesh.vertices);
    std::vector<uint32_t> cumul(freqs.size(), 0);
    uint32_t totalFreq = 0;
    for (size_t i = 0; i < freqs.size(); i++) {
        cumul[i] = totalFreq;
        totalFreq += freqs[i];
    }

    uint32_t state = 0;
    for (int i = 3; i >= 0; i--) {
        state = (state << 8) | cumul[static_cast<uint8_t>(i_mesh.vertices[0][i])];
    }

    std::vector<uint8_t> output;
    for (const auto& vertex : i_mesh.vertices) {
        for (int i = 0; i < 3; i++) {
            uint8_t symbol = static_cast<uint8_t>(vertex[i]);
            uint32_t freq = freqs[symbol];
            uint32_t cumulFreq = cumul[symbol];
            uint32_t newState = state / (totalFreq / freq);int32_t diff = state - newState * (totalFreq / freq);
            if (newState >= freq * 31) {
                // Flush state
                for (int i = 0; i < 4; i++) {
                    output.push_back(state & 0xFF);
                    state >>= 8;
                }
                state = 0;
            }
            state = newState * freq + diff + cumulFreq;
        }
    }

    for (int i = 0; i < 4; i++) {
        output.push_back(state & 0xFF);
        state >>= 8;
    }

    std::cout << "fin rANS" << std::endl;

    std::cout << "taux de compression: " << (float) i_mesh.vertices.size() / (float) output.size()  << std::endl;

    revRans(output);*/

    std::vector<uint8_t> data;

    //calcul des fréquences de tous les symboles
    auto freqs = computeFrequencies(i_mesh.vertices);

    //fonction cumul des fréquences pour calculs
    std::vector<uint32_t> cumul(freqs.size(), 0);
    uint32_t totalFreq = 0;
    for (size_t i = 0; i < freqs.size(); i++) {
        cumul[i] = totalFreq;
        totalFreq += freqs[i];
    }

    uint32_t state = 0;
    for (int i = 3; i >= 0; i--) {
        state = (state << 8) | cumul[static_cast<uint8_t>(i_mesh.vertices[0][i])];
    }

    //formule rANS
    for (const auto &vertex: i_mesh.vertices) {
        for (int i = 0; i < 3; i++) {
            uint8_t symbol = static_cast<uint8_t>(vertex[i]);
            uint32_t freq = freqs[symbol];
            uint32_t cumulFreq = cumul[symbol];
            uint32_t newState = state / (totalFreq / freq);
            state = newState;
            if (state < 8) {
                data.push_back(state & 0xff);
                state = state >> 8;
            }
            state = (state << 8) | (newState - freq * cumulFreq);
        }
    }

    while (state > 0) {
        data.push_back(state & 0xff);
        state = state >> 8;
    }

    std::cout << "taux de compression: " << (float) i_mesh.vertices.size() / (float) data.size() << std::endl;

    revRans(data);
}

/**
 * utilisation bibliothèque ryg_rans
 **/

struct SymbolStats {
    uint32_t freqs[256];
    uint32_t cum_freqs[257];

    void count_freqs(uint8_t const *in, size_t nbytes);

    void calc_cum_freqs();

    void normalize_freqs(uint32_t target_total);
};

void SymbolStats::count_freqs(uint8_t const *in, size_t nbytes) {
    for (int i = 0; i < 256; i++)
        freqs[i] = 0;

    for (size_t i = 0; i < nbytes; i++)
        freqs[in[i]]++;
}

void SymbolStats::calc_cum_freqs() {
    cum_freqs[0] = 0;
    for (int i = 0; i < 256; i++)
        cum_freqs[i + 1] = cum_freqs[i] + freqs[i];
}

void SymbolStats::normalize_freqs(uint32_t target_total) {
    calc_cum_freqs();
    uint32_t cur_total = cum_freqs[256];

    // resample distribution based on cumulative freqs
    for (int i = 1; i <= 256; i++)
        cum_freqs[i] = ((uint64_t) target_total * cum_freqs[i]) / cur_total;

    // if we nuked any non-0 frequency symbol to 0, we need to steal
    // the range to make the frequency nonzero from elsewhere.
    //
    // this is not at all optimal, i'm just doing the first thing that comes to mind.
    for (int i = 0; i < 256; i++) {
        if (freqs[i] && cum_freqs[i + 1] == cum_freqs[i]) {
            // symbol i was set to zero freq

            // find best symbol to steal frequency from (try to steal from low-freq ones)
            uint32_t best_freq = ~0u;
            int best_steal = -1;
            for (int j = 0; j < 256; j++) {
                uint32_t freq = cum_freqs[j + 1] - cum_freqs[j];
                if (freq > 1 && freq < best_freq) {
                    best_freq = freq;
                    best_steal = j;
                }
            }
            assert(best_steal != -1);

            // and steal from it!
            if (best_steal < i) {
                for (int j = best_steal + 1; j <= i; j++)
                    cum_freqs[j]--;
            } else {
                assert(best_steal > i);
                for (int j = i + 1; j <= best_steal; j++)
                    cum_freqs[j]++;
            }
        }
    }

    // calculate updated freqs and make sure we didn't screw anything up
    for (int i = 0; i < 256; i++) {
        if (freqs[i] == 0)
            assert(cum_freqs[i + 1] == cum_freqs[i]);
        else
            assert(cum_freqs[i + 1] > cum_freqs[i]);

        // calc updated freq
        freqs[i] = cum_freqs[i + 1] - cum_freqs[i];
    }
}

/**
 * Encodage et décodage rANS avec bibliothèque 
 **/
void rans2(Mesh &i_mesh, int qp) {
    drawQuantif(i_mesh, qp);
    size_t in_size = i_mesh.vertices.size() * 3;
    uint8_t in_bytes[in_size];
    for (size_t i = 0; i < mesh.vertices.size(); i++) {
        for (int j = 0; j < 3; j++) {
            in_bytes[i * 3 + j] = static_cast<uint8_t>(mesh.vertices[i][j]);
        }
    }
    //car chaque élément du tableau est un uint8_t qui représente 8bits
    int sizeInBits = in_size * 8;
    std::cout << sizeInBits << std::endl;
    static const uint32_t prob_bits = static_cast<uint32_t>(qp);
    static const uint32_t prob_scale = 1 << prob_bits;

    SymbolStats stats;
    stats.count_freqs(in_bytes, in_size);
    stats.normalize_freqs(prob_scale);

    // cumlative->symbol table
    // this is super brute force
    uint8_t cum2sym[prob_scale];
    for (int s = 0; s < 256; s++)
        for (uint32_t i = stats.cum_freqs[s]; i < stats.cum_freqs[s + 1]; i++)
            cum2sym[i] = s;

    static size_t out_max_size = 32 << 20; // 32MB
    uint8_t *out_buf = new uint8_t[out_max_size];
    uint8_t *dec_bytes = new uint8_t[in_size];

    // try rANS encode
    uint8_t *rans_begin;
    RansEncSymbol esyms[256];
    RansDecSymbol dsyms[256];

    for (int i = 0; i < 256; i++) {
        RansEncSymbolInit(&esyms[i], stats.cum_freqs[i], stats.freqs[i], prob_bits);
        RansDecSymbolInit(&dsyms[i], stats.cum_freqs[i], stats.freqs[i]);
    }

    // ---- regular rANS encode/decode. Typical usage.

    memset(dec_bytes, 0xcc, in_size);

    printf("rANS encode:\n");
    for (int run = 0; run < 5; run++) {
        RansState rans;
        RansEncInit(&rans);

        uint8_t *ptr = out_buf + out_max_size; // *end* of output buffer
        for (size_t i = in_size; i > 0; i--) { // NB: working in reverse!
            int s = in_bytes[i - 1];
            RansEncPutSymbol(&rans, &ptr, &esyms[s]);
        }
        RansEncFlush(&rans, &ptr);
        rans_begin = ptr;

    }
    printf("rANS: %d Bytes\n", (int) (out_buf + out_max_size - rans_begin));
    printf("rANS: %d Bits\n", (int) (out_buf + out_max_size - rans_begin) * 8);
    std::cout << "Taux de compression: " << (double) sizeInBits / (double) ((out_buf + out_max_size - rans_begin) * 8)
              << std::endl;
    // try rANS decode
    for (int run = 0; run < 5; run++) {
        RansState rans;
        uint8_t *ptr = rans_begin;
        RansDecInit(&rans, &ptr);

        for (size_t i = 0; i < in_size; i++) {
            uint32_t s = cum2sym[RansDecGet(&rans, prob_bits)];
            dec_bytes[i] = (uint8_t) s;
            RansDecAdvanceSymbol(&rans, &ptr, &dsyms[s], prob_bits);
        }

    }
}

void key(unsigned char keyPressed, int x, int y) {
    switch (keyPressed) {
        case 'f':
            if (fullScreen == true) {
                glutReshapeWindow(SCREENWIDTH, SCREENHEIGHT);
                fullScreen = false;
            } else {
                glutFullScreen();
                fullScreen = true;
            }
            break;


        case 'w':
            GLint polygonMode[2];
            glGetIntegerv(GL_POLYGON_MODE, polygonMode);
            if (polygonMode[0] != GL_FILL)
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            else
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            break;


        case 'n': //Press n key to display normals
            display_normals = !display_normals;
            break;

        case '1': //Toggle loaded mesh display
            display_loaded_mesh = !display_loaded_mesh;
            break;

        case 'q':
            drawQuantif(mesh, 5);
            break;

        case 'x':
            drawRevQuantif(mesh, 5);
            break;

        case 'r':
            RMSEtrue(mesh);
            break;

        case 'a':
            rans(mesh, 5);
            break;

        case 'z':
            rans2(mesh, 5);
            break;

        case 'b': {
            Vec3 nouv = calculerBarycentre(mesh);
            std::cout << "barycentre: " << nouv << std::endl;
            Vec3 sphere = cartesianToSpherical(nouv);
            std::cout << "coords sphériques: " << sphere << std::endl;
            std::cout << "retour en coords cartésiennes: " << sphericalToCartesian(sphere) << std::endl;
            float min, max;
            getExtrema(mesh,min,max);
            std::cout << "min et max radiale: " << min << " et " << max << std::endl;

            //nbBins = longueur message
            int k = message.length();
            float alpha = 0.1f;
            float force = 0.05f;
            tatouage(mesh, alpha, k, force);
            std::cout << "message: " << message << std::endl;
            /*std::string message2 = extraireMessage(mesh, k, alpha);
            std::cout << "message extrait: " << message2 << std::endl;*/
            extraireMessage(mesh, alpha, k);
            break;
        }

        default:
            break;
    }
    idle();
}

void mouse(int button, int state, int x, int y) {
    if (state == GLUT_UP) {
        mouseMovePressed = false;
        mouseRotatePressed = false;
        mouseZoomPressed = false;
    } else {
        if (button == GLUT_LEFT_BUTTON) {
            camera.beginRotate(x, y);
            mouseMovePressed = false;
            mouseRotatePressed = true;
            mouseZoomPressed = false;
        } else if (button == GLUT_RIGHT_BUTTON) {
            lastX = x;
            lastY = y;
            mouseMovePressed = true;
            mouseRotatePressed = false;
            mouseZoomPressed = false;
        } else if (button == GLUT_MIDDLE_BUTTON) {
            if (mouseZoomPressed == false) {
                lastZoom = y;
                mouseMovePressed = false;
                mouseRotatePressed = false;
                mouseZoomPressed = true;
            }
        }
    }
    idle();
}

void motion(int x, int y) {
    if (mouseRotatePressed == true) {
        camera.rotate(x, y);
    } else if (mouseMovePressed == true) {
        camera.move((x - lastX) / static_cast<float>(SCREENWIDTH), (lastY - y) / static_cast<float>(SCREENHEIGHT), 0.0);
        lastX = x;
        lastY = y;
    } else if (mouseZoomPressed == true) {
        camera.zoom(float(y - lastZoom) / SCREENHEIGHT);
        lastZoom = y;
    }
}

void reshape(int w, int h) {
    camera.resize(w, h);
}


int main(int argc, char **argv) {
    if (argc > 2) {
        exit(EXIT_FAILURE);
    }
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(SCREENWIDTH, SCREENHEIGHT);
    window = glutCreateWindow("TP HAI714I");

    init();
    glutIdleFunc(idle);
    glutDisplayFunc(display);
    glutKeyboardFunc(key);
    glutReshapeFunc(reshape);
    glutMotionFunc(motion);
    glutMouseFunc(mouse);
    key('?', 0, 0);



    //Look into data to find other meshes
    openOFF("data/camel_n.off", mesh.vertices, mesh.normals, mesh.triangles);
    mesh.buildVertexArray();
    mesh.buildTriangleArray();
    mesh.buildNormalArray();
    mesh.buildColorArray();

    glutMainLoop();
    return EXIT_SUCCESS;
}

