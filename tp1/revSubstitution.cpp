//
// Created by florian.ravet-lecourt@etu.umontpellier.fr on 11/09/23.
//

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>
#include <vector>
#include "image_ppm.h"

using namespace std;

int main(int argc, char *argv[]) {

    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille;

    if (argc != 3) {
        printf("Usage: ImageIn.pgm ImageOut.txt \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf(argv[2], "%s", cNomImgEcrite);

    OCTET *ImgIn, *ImgOut;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    for (int cle = 1; cle < 256; cle++) {
        double entropy = 0;
        srand(cle);
        vector<int> k(nTaille);
        for (int i = 0; i < nTaille; i++) {
            k[i] = rand() % 256;
        }

        for (int i = 1; i < nTaille - 1; i++) {
            ImgOut[i] = (ImgIn[i] - ImgIn[i - 1] - k[i]) % 256;
        }

        int Histo[256];
        double proba[256];
        for (int i = 0; i < 256; i++) {
            Histo[i] = 0;
            proba[i] = 0;
        }

        for (int i = 0; i < nH; i++) {
            for (int j = 0; j < nW; j++) {
                Histo[ImgOut[i * nW + j]]++;
            }
        }

        for (int i = 0; i < 256; i++) {
            proba[i] = (double) Histo[i] / (double) nTaille;
        }

        entropy = 0;
        for (int i = 0; i < 256; i++) {
            if (proba[i] != 0) {
                entropy += (-1) * (proba[i] * std::log2(proba[i]));
            }
        }
        cout << "entropie: " << entropy << endl;
        if (entropy < 7.9) {
            cout << "clé: " << cle << endl;
            break;
        }
    }


    string outputFileName = string(cNomImgEcrite) + "_revsub.pgm";
    char outputFileNameChar[250];
    strcpy(outputFileNameChar, outputFileName.c_str());
    ecrire_image_pgm(outputFileNameChar, ImgOut, nH, nW);

    free(ImgIn);
    free(ImgOut);

    return 0;
}