// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[]){

  char cNomImgLue[250], cNomTxt[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.txt \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomTxt);

   OCTET *ImgIn; //, *ImgOut;
   
   lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   allocation_tableau(ImgIn, OCTET, nTaille);
   lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
   //allocation_tableau(ImgOut, OCTET, nTaille);
	
   int Histo[256];
   for(int i=0; i < 256; i++){
      Histo[i] = 0;
   }

  //for(int g; g < 256; g++){
    for (int i=0; i < nH; i++){
      for (int j=0; j < nW; j++){
         /*if (ImgIn[i*nW+j] == g)*/ Histo[ImgIn[i*nW+j]]++; 
      }
    }
    //cout << g << " " << Histo[g] << endl;
  //}

  fstream fichier;
  fichier.open(cNomTxt, ios::out | ios::trunc);
  if(fichier.is_open()){
    for(int i=0; i < 256; i++){
      fichier << i << " " << Histo[i] << "\n";
      cout << i << " " << Histo[i] << "\n" << endl;
   }
   fichier.close();
  }

   //ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); //free(ImgOut);

   return 0;
}
