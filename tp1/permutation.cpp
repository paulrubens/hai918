#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <map>
#include "image_ppm.h"

using namespace std;

int main(int argc, char *argv[]) {

    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, k;

    if (argc != 4) {
        printf("Usage: ImageIn.pgm ImageOut.txt clé \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);
    sscanf (argv[2],"%s", cNomImgEcrite);
    sscanf(argv[3], "%d", &k);

    OCTET *ImgIn, *ImgOut, *ImgInv;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);

    srand(k);

    map<int, int> pos_permute; //utilisé pour mémoriser les positions permutées

    for (int i = 0; i < nTaille; i++) {
        int pos = rand() % nTaille;
        while (pos_permute.find(pos) != pos_permute.end()) {
            pos = rand() % nTaille;
        }
        pos_permute[pos] = i;
    }

    for (const auto &pair : pos_permute) {
        ImgOut[pair.first] = ImgIn[pair.second];
    }

    allocation_tableau(ImgInv, OCTET, nTaille);

    for (const auto &pair : pos_permute) {
        ImgInv[pair.second] = ImgOut[pair.first];
    }

    ecrire_image_pgm(strcat(strcat(strcat(cNomImgEcrite, "_"), to_string(k).c_str()), ".pgm"), ImgOut,  nH, nW);
    ecrire_image_pgm(strcat(strcat(strcat(cNomImgEcrite, "_"), to_string(k).c_str()), "_inv.pgm"), ImgInv,  nH, nW);

    free(ImgIn); free(ImgOut);

    return 0;
}
