// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include "image_ppm.h"

using namespace std;

int main(int argc, char *argv[]) {

    char cNomImgLue[250];
    int nH, nW, nTaille;

    if (argc != 2) {
        printf("Usage: ImageIn.pgm ImageOut.txt \n");
        exit(1);
    }

    sscanf(argv[1], "%s", cNomImgLue);

    OCTET *ImgIn;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    //allocation_tableau(ImgOut, OCTET, nTaille);

    int Histo[256];
    double proba[256];
    for (int i = 0; i < 256; i++) {
        Histo[i] = 0;
        proba[i] = 0;
    }

    for (int i = 0; i < nH; i++) {
        for (int j = 0; j < nW; j++) {
            Histo[ImgIn[i * nW + j]]++;
        }
    }

    for (int i = 0; i < 256; i++) {
        proba[i] = (double) Histo[i] / (double) nTaille;
    }

    std::cout << "calcul entropie" << std::endl;

    double entropy = 0;
    for(int i=0; i < 256; i++){
        if(proba[i] != 0 ){
            entropy += (-1) * (proba[i] * std::log2(proba[i]));
        }
    }

    std::cout << "entropie: " << entropy << " bits/pixel" << std::endl;

    free(ImgIn); //free(ImgOut);

    return 0;
}
