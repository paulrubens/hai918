//
// Created by florian.ravet-lecourt@etu.umontpellier.fr on 25/09/23.
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
#include <string>
#include <vector>

using namespace std;

double calculer_PSNR(OCTET *Img1, OCTET *Img2, int taille) {
    double EQMt = 0;
    for (int i = 0; i < taille; i++) {
        double val1 = static_cast<double>(Img1[i]);
        double val2 = static_cast<double>(Img2[i]);
        EQMt += ((val1 - val2) * (val1 - val2));
    }
    EQMt /= taille;

    if (EQMt == 0) {
        return INFINITY;
    }
    return 10 * std::log10(std::pow(255, 2) / EQMt);
}

double calculer_entropie(OCTET *Img, int taille) {
    int Histo[256] = {0};
    double proba[256] = {0.0};

    for (int i = 0; i < taille; i++) {
        Histo[Img[i]]++;
    }

    for (int i = 0; i < 256; i++) {
        proba[i] = (double)Histo[i] / (double)taille;
    }

    double entropy = 0;
    for (int i = 0; i < 256; i++) {
        if (proba[i] != 0) {
            entropy += -proba[i] * std::log2(proba[i]);
        }
    }
    return entropy;
}

int main(int argc, char* argv[]){

    char cNomImgLue[250], cNomImgEcrite[250];
    int nH, nW, nTaille, k;

    if (argc != 4)
    {
        printf("Usage: ImageIn.pgm ImageOut.txt k \n");
        exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cNomImgEcrite);
    sscanf (argv[3],"%d",&k);

    OCTET *ImgIn, *ImgOut, *ImgFin;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgOut, OCTET, nTaille);
    allocation_tableau(ImgFin, OCTET, nTaille);

    char message [nTaille];
    srand(time(NULL));

    for(int i = 0; i < nTaille; i++){
        message[i] = rand() % 2; //message aléatoire contenant des 0 ou des 1
    }

    //reconstruire image
    for (int i = 0; i < nH; i++) {
        for (int j = 0; j < nW; j++) {

            char originalBits = ImgIn[i*nW+j];
            char messageBit = message[i*nW+j];
            char mask = ~(1 << k); //plus de détails dans le rapport

            ImgFin[i * nW + j] = (originalBits & mask) | (messageBit << k);
            // ~ -> NOT operator
            // & AND operator
            // << left shift
            // ^ XOR operator
        }
    }

    //plan binaire
    for(int i = 0; i < nH; i++){
        for(int j = 0; j < nW; j++){
            ImgOut[i*nW+j] = ((ImgFin[i*nW+j] & (char)std::pow(2,k))>0)*255;
        }
    }


    // Calcul et affichage du PSNR
    double psnr = calculer_PSNR(ImgIn, ImgOut, nTaille);
    double psnr2 = calculer_PSNR(ImgIn, ImgFin, nTaille);
    std::cout << "PSNR entre l'image originale et le plan binaire: " << psnr << " dB" << std::endl;
    std::cout << "PSNR entre l'image originale et l'image modifiée': " << psnr2 << " dB" << std::endl;

    // Calcul et affichage de l'entropie
    double entropyImgIn = calculer_entropie(ImgIn, nTaille);
    double entropyImgOut = calculer_entropie(ImgOut, nTaille);
    double entropyImgFin = calculer_entropie(ImgFin, nTaille);
    std::cout << "Entropie de l'image originale: " << entropyImgIn << " bits/pixel" << std::endl;
    std::cout << "Entropie du plan binaire: " << entropyImgOut << " bits/pixel" << std::endl;
    std::cout << "Entropie de l'image modifiée: " << entropyImgFin << " bits/pixel" << std::endl;

    char tmpNomImgEcrite[250];
    sprintf(tmpNomImgEcrite, "%s_insert_%d.pgm", cNomImgEcrite, k);
    ecrire_image_pgm(tmpNomImgEcrite, ImgOut,  nH, nW);

    sprintf(tmpNomImgEcrite, "%s_insert_%d_fin.pgm", cNomImgEcrite, k);
    ecrire_image_pgm(tmpNomImgEcrite, ImgFin,  nH, nW);

    sprintf(tmpNomImgEcrite, "%s_insert_%d_message.txt", cNomImgEcrite, k);
    std::ofstream fichierTexte(tmpNomImgEcrite);
    for(int i = 0; i < nTaille; i++) {
        fichierTexte << static_cast<int>(message[i]);
    }
    fichierTexte.close();

    free(ImgIn); free(ImgOut); free(ImgFin);

    return 0;
}
