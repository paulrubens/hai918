// test_couleur.cpp : Seuille une image en niveau de gris

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"

using namespace std;

int main(int argc, char* argv[]){

  char cNomImgLue[250], cNomTxt[250];
  int nH, nW, nTaille;
  
  if (argc != 3) 
     {
       printf("Usage: ImageIn.pgm ImageOut.txt \n"); 
       exit (1) ;
     }
   
   sscanf (argv[1],"%s",cNomImgLue) ;
   sscanf (argv[2],"%s",cNomTxt);

   OCTET *ImgIn; //, *ImgOut;
   
   lire_nb_lignes_colonnes_image_ppm(cNomImgLue, &nH, &nW);
   nTaille = nH * nW;
  
   int nTaille3 = nTaille * 3;
   allocation_tableau(ImgIn, OCTET, nTaille3);
   lire_image_ppm(cNomImgLue, ImgIn, nH * nW);
   //allocation_tableau(ImgOut, OCTET, nTaille);
	
   int HistoR[256];
   int HistoB[256];
   int HistoV[256];
   for(int i=0; i < 256; i++){
      HistoR[i] = 0;
      HistoB[i] = 0;
      HistoV[i] = 0;
   }

  for (int i=0; i < nTaille3; i+=3){
    HistoR[ImgIn[i]]++;
    HistoB[ImgIn[i+1]]++;
    HistoV[ImgIn[i+2]]++;
  }

  fstream fichier;
  fichier.open(cNomTxt, ios::out | ios::trunc);
  if(fichier.is_open()){
    for(int i=0; i < 256; i++){
      fichier << i << " " << HistoR[i] << " " << HistoV[i] << " " << HistoB[i] << "\n";
      cout << i << " " << HistoR[i] << " " << HistoV[i] << " " << HistoB[i] << "\n" << endl;
   }
   fichier.close();
  }

   //ecrire_image_pgm(cNomImgEcrite, ImgOut,  nH, nW);
   free(ImgIn); //free(ImgOut);

   return 0;
}
