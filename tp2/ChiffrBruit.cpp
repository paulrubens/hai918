//
// Created by florian.ravet-lecourt@etu.umontpellier.fr on 18/09/23.
//

#include <stdio.h>
#include <iostream>
#include <fstream>
#include "image_ppm.h"
#include <string>
#include <vector>
#include "plusaes.hpp"

using namespace std;

double calculer_PSNR(OCTET *Img1, OCTET *Img2, int taille) {
    double EQMt = 0;
    for (int i = 0; i < taille; i++) {
        double val1 = static_cast<double>(Img1[i]);
        double val2 = static_cast<double>(Img2[i]);
        EQMt += ((val1 - val2) * (val1 - val2));
    }
    EQMt /= taille;

    if (EQMt == 0) {
        return INFINITY;
    }
    return 10 * std::log10(std::pow(255, 2) / EQMt);
}

int main(int argc, char* argv[]){

    char cNomImgLue[250], cNomImgEcrite[250], cBruit[250];
    int nH, nW, nTaille;

    if (argc != 4)
    {
        printf("Usage: ImageIn.pgm ImageBruit.pgm ImageOut.txt \n");
        exit (1) ;
    }

    sscanf (argv[1],"%s",cNomImgLue) ;
    sscanf (argv[2],"%s",cBruit);
    sscanf (argv[3],"%s",cNomImgEcrite);

    OCTET *ImgIn, *ImgOutCBC, *ImgBruit, *ImgInvCBC;
    OCTET *ImgOutECB, *ImgInvECB;
    OCTET *ImgOutOFB, *ImgInvOFB;
    OCTET *ImgOutCFB, *ImgInvCFB;
    OCTET *ImgOutCTR, *ImgInvCTR;

    lire_nb_lignes_colonnes_image_pgm(cNomImgLue, &nH, &nW);
    nTaille = nH * nW;

    allocation_tableau(ImgIn, OCTET, nTaille);
    lire_image_pgm(cNomImgLue, ImgIn, nH * nW);
    allocation_tableau(ImgBruit, OCTET, nTaille);
    lire_image_pgm(cBruit, ImgBruit, nH * nW);
    allocation_tableau(ImgOutCBC, OCTET, nTaille);
    allocation_tableau(ImgInvCBC, OCTET, nTaille);
    allocation_tableau(ImgOutECB, OCTET, nTaille);
    allocation_tableau(ImgInvECB, OCTET, nTaille);
    allocation_tableau(ImgOutOFB, OCTET, nTaille);
    allocation_tableau(ImgInvOFB, OCTET, nTaille);
    allocation_tableau(ImgOutCFB, OCTET, nTaille);
    allocation_tableau(ImgInvCFB, OCTET, nTaille);
    allocation_tableau(ImgOutCTR, OCTET, nTaille);
    allocation_tableau(ImgInvCTR, OCTET, nTaille);

    std::vector<unsigned char> raw_data(ImgBruit, ImgBruit + nTaille);
    const std::vector<unsigned char> key = plusaes::key_from_string(&"EncryptionKey128"); // 16-char = 128-bit
    const unsigned char iv[16] = {
            0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07,
            0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F,
    };

    // CBC
    const unsigned long encrypted_size = plusaes::get_padded_encrypted_size(raw_data.size());
    std::vector<unsigned char> encrypted(encrypted_size);
    plusaes::encrypt_cbc((unsigned char*)raw_data.data(), raw_data.size(), &key[0], key.size(), &iv, &encrypted[0], encrypted.size(), true);
    std::copy(encrypted.begin(), encrypted.begin() + nTaille, ImgOutCBC);
    unsigned long padded_size = 0;
    std::vector<unsigned char> decrypted(encrypted_size);
    plusaes::decrypt_cbc(&encrypted[0], encrypted.size(), &key[0], key.size(), &iv, &decrypted[0], decrypted.size(), &padded_size);
    std::copy(decrypted.begin(), decrypted.begin() + nTaille, ImgInvCBC);

    //ECB
    plusaes::encrypt_ecb((unsigned char*)raw_data.data(), raw_data.size(), &key[0], key.size(), &encrypted[0], encrypted.size(), true);
    std::copy(encrypted.begin(), encrypted.begin() + nTaille, ImgOutECB);
    plusaes::decrypt_ecb(&encrypted[0], encrypted.size(), &key[0], key.size(), &decrypted[0], decrypted.size(), &padded_size);
    std::copy(decrypted.begin(), decrypted.begin() + nTaille, ImgInvECB);

    //OFB
    plusaes::encrypt_ofb((unsigned char*)raw_data.data(), raw_data.size(), &key[0], key.size(), &iv, &encrypted[0], encrypted.size());
    std::copy(encrypted.begin(), encrypted.begin() + nTaille, ImgOutOFB);
    plusaes::decrypt_ofb(&encrypted[0], encrypted.size(), &key[0], key.size(), &iv, &decrypted[0], decrypted.size());
    std::copy(decrypted.begin(), decrypted.begin() + nTaille, ImgInvOFB);

    //CFB
    plusaes::encrypt_cfb((unsigned char*)raw_data.data(), raw_data.size(), &key[0], key.size(), &iv, &encrypted[0], encrypted.size());
    std::copy(encrypted.begin(), encrypted.begin() + nTaille, ImgOutCFB);
    plusaes::decrypt_cfb(&encrypted[0], encrypted.size(), &key[0], key.size(), &iv, &decrypted[0], decrypted.size());
    std::copy(decrypted.begin(), decrypted.begin() + nTaille, ImgInvCFB);

    //CTR
    plusaes::crypt_ctr((unsigned char*)raw_data.data(), raw_data.size(), &key[0], key.size(), &iv);
    std::copy(raw_data.begin(), raw_data.begin() + nTaille, ImgOutCTR);
    plusaes::crypt_ctr((unsigned char*)raw_data.data(), raw_data.size(), &key[0], key.size(), &iv);
    std::copy(raw_data.begin(), raw_data.begin() + nTaille, ImgInvCTR);

    // Calcul et affichage du PSNR
    double psnr1 = calculer_PSNR(ImgIn, ImgBruit, nTaille);
    double psnr2 = calculer_PSNR(ImgIn, ImgInvCBC, nTaille);
    double psnr3 = calculer_PSNR(ImgIn, ImgInvECB, nTaille);
    double psnr4 = calculer_PSNR(ImgIn, ImgInvOFB, nTaille);
    double psnr5 = calculer_PSNR(ImgIn, ImgInvCFB, nTaille);
    double psnr6 = calculer_PSNR(ImgIn, ImgInvCTR, nTaille);
    std::cout << "PSNR entre l'image originale et l'image bruitée: " << psnr1 << " dB" << std::endl;
    std::cout << "PSNR entre l'image originale et l'image déchiffrée CBC: " << psnr2 << " dB" << std::endl;
    std::cout << "PSNR entre l'image originale et l'image déchiffrée ECB: " << psnr3 << " dB" << std::endl;
    std::cout << "PSNR entre l'image originale et l'image déchiffrée OFB: " << psnr4 << " dB" << std::endl;
    std::cout << "PSNR entre l'image originale et l'image déchiffrée CFB: " << psnr5 << " dB" << std::endl;
    std::cout << "PSNR entre l'image originale et l'image déchiffrée CTR: " << psnr6 << " dB" << std::endl;

    char tmpNomImgEcrite[250];

    sprintf(tmpNomImgEcrite, "%s_%s_inv.pgm", cNomImgEcrite, "cbc");
    ecrire_image_pgm(tmpNomImgEcrite, ImgInvCBC,  nH, nW);

    sprintf(tmpNomImgEcrite, "%s_%s_inv.pgm", cNomImgEcrite, "ecb");
    ecrire_image_pgm(tmpNomImgEcrite, ImgInvECB,  nH, nW);

    sprintf(tmpNomImgEcrite, "%s_%s_inv.pgm", cNomImgEcrite, "ofb");
    ecrire_image_pgm(tmpNomImgEcrite, ImgInvOFB,  nH, nW);

    sprintf(tmpNomImgEcrite, "%s_%s_inv.pgm", cNomImgEcrite, "cfb");
    ecrire_image_pgm(tmpNomImgEcrite, ImgInvCFB,  nH, nW);

    sprintf(tmpNomImgEcrite, "%s_%s_inv.pgm", cNomImgEcrite, "ctr");
    ecrire_image_pgm(tmpNomImgEcrite, ImgInvCTR,  nH, nW);

    free(ImgIn); free(ImgOutCBC); free(ImgInvCBC);
    free(ImgOutECB); free(ImgInvECB);
    free(ImgOutOFB); free(ImgInvOFB);
    free(ImgOutCFB); free(ImgInvCFB);
    free(ImgOutCTR); free(ImgInvCTR);

    return 0;
}
